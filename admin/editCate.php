
<?php
session_start();
require_once '../database/connect.php';
?>
//1. lấy dữ liệu đưa lên form cho người dùng sửa
//2. lấy dữ liệu người dùng post từ form và kiểm tra hợp lệ
//3. lưu vào CSDL
<?php
if(!isset($_GET['id'])){
    $_SESSION['err'] = "Bạn chưa chọn Role để sửa";
    header("Location:category.php");
}
$id = intval($_GET['id']);
try{
    $stmt = $objConn->prepare("SELECT * FROM category WHERE id = $id ");
    $stmt->execute();
    
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    $item = $stmt->fetch();

    if(empty($item)){
        $_SESSION['err'] = 'dsdsds '. $id;
        header("Location:category.php");
    }  
}catch(PDOException $e){
    echo "<br>Loi truy van CSDL: ".$e->getMessage();
}

// chuc nang sua
$err =[];
if(isset($_POST['btnsave'])){
    $name = $_POST['txtname'];

    // kiểm tra
    if(empty($name)){
        $err[] = "Bạn chưa nhập tên danh mục";
    }

    if(empty($err)){
        // không có lỗi nhập sai dữ liệu
        // ghi vào csdl: UPDATE tb_role SET name=:post_name WHERE id=:get_id
        try{
            $stmt = $objConn->prepare("UPDATE category SET name=:post_name WHERE id=:get_id");
            // gắn dữ liệu vào tham số
            $stmt->bindParam(":post_name", $name);
            $stmt->bindParam(":get_id", $id);
            // thực thi câu lệnh
            $stmt->execute();
            header("Location:category.php");

        }catch(PDOException $e){
            $err[] = "Loi truy van CSDL: ". $e->getMessage();
        }
 
    }  
} 
?>
<?php
    require_once ('header.php');
?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <!-- hien thi tung chuc nang cua trang quan tri START-->
       <!-- form sua danh muc -->
            <div class="container">
            
            <form method="post" action="" class="mt-5">
            <p style='color:red'> <?php echo implode('<br>',$err ); ?> </p>
              <div class="form-group">
                <label for="">Tên danh mục</label>
                <input type="text" name="txtname" class="form-control" value="<?php echo $item['name'] ?>" >
                
              </div>
              <button  name="btnsave" class="btn btn-primary">Cập nhật danh mục</button>
            </form>

         
            </div>
            
    </main>
</div>


      

</body>
</html>