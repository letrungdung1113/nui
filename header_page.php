<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="public/assets/css/style.css" />
    <title>index</title>
  </head>
  <body>
      <!-- header -->
      <div class="header">
  
    <div class="container">
      <!-- navbar -->
      <div class="navbar">
        <div class="logo">
          <img src="public/assets/images/logo.png" alt="logo" width="125px" />
        </div>
        <nav>
          <ul id="MenuItems">
            <li><a href="index.php">Trang chủ</a></li>
            <li><a href="shop.php">Sản phẩm</a></li>
            <li><a href="about-us.html">About</a></li>
            <li><a href="contact.html">Contact</a></li>
            <li><a href="account.php">Tài khoản</a></li>
          </ul>
        </nav>
         <!-- cart -->
     <a href="cart.html"><img src="public/assets/images/cart.png" alt="cart" width="35px" height="35px"></a> 
      <img src="assets/images/menu.png" alt="menu" class="menu-icon" onclick="menutoggle()">
    </div>
     