<?php
  require_once ('../database/connect.php');
?>
<?php
    require_once ('header.php');
?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <!-- hien thi tung chuc nang cua trang quan tri START-->
       <!-- form them danh muc -->
       <?php
              $msg = "";
              $err = "";
              if(isset($_POST['btnadd'])){
                $name = $_POST['txtname'];
                // kiem tra form
                if(empty($name)){
                  $err = "Bạn chưa nhập tên role";
                }
                if(strlen($name) > 50){
                  $err = "Tên danh mục quá dài";
                }

                if(empty($err)){
                  // không có lỗi nhập sai dữ liệu
                  // ghi vào csdl
                  try{
                      $stmt = $objConn->prepare("INSERT INTO role(name) VALUES(:post_name)");
                      // gắn dữ liệu vào tham số
                      $stmt->bindParam(":post_name", $name);
                      // thực thi câu lệnh
                      $stmt->execute();
                      
                      header('Location:role.php');
          
                  }catch(PDOException $e){
                      $err[] = "Loi truy van CSDL: ". $e->getMessage();
                  }
           
              } 
              }
              
            ?>
            <div class="container">
            <form method="post" action="" class="mt-5">
              
              <div class="form-group">
                <label for="">Tên role</label>
                <input type="text" name="txtname" class="form-control" >
                <p class="text-danger"> <?php echo ($err); ?> </p>
              </div>
              
              <button type="submit" name="btnadd" class="btn btn-primary">Thêm role</button>
            </form>
            </div>
            
    </main>
</div>

      

</body>
</html>