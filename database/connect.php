<?php
$host = 'localhost';
$db_user = 'root';
$db_passwd = '';
$db_name = 'phpbase';

try{
    $objConn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_passwd);

    $objConn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

    echo "";

}catch(Exception $e){
    die( $e->getMessage()  );
}