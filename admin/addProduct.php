<?php
  require_once ('../database/connect.php');
?>
<?php
    require_once ('header.php');
?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <!-- hien thi tung chuc nang cua trang quan tri START-->
       <!-- form them danh muc -->
       <?php
              $err = "";
              if(isset($_POST['btnadd'])){
                $name = $_POST['txtname'];
                // kiem tra form
                if(empty($name)){
                  $err = "Bạn chưa nhập tên danh mục";
                }
                if(strlen($name) > 50){
                  $err = "Tên danh mục quá dài";
                }

                if(empty($err)){
                  // không có lỗi nhập sai dữ liệu
                  // ghi vào csdl
                  try{
                      $stmt = $objConn->prepare("INSERT INTO category(name) VALUES(:post_name)");
                      // gắn dữ liệu vào tham số
                      $stmt->bindParam(":post_name", $name);
                      // thực thi câu lệnh
                      $stmt->execute();
                      
                      header('Location:category.php');
          
                  }catch(PDOException $e){
                      $err[] = "Loi truy van CSDL: ". $e->getMessage();
                  }
           
              } 
              }
              
            ?>
            <div class="container">
            <form method="post" action="" class="mt-5" enctype="multipart/form-data">
              
              <div class="form-group">
              <p class="text-danger"> <?php echo ($err); ?> </p>

                <label for="">Danh mục</label>
                <input type="text" name="txtname" class="form-control" >
              </div>
              <div class="form-group">
                <label for="">Tiêu đề sản phẩm</label>
                <input type="text" name="txtname" class="form-control" >
                </div>
                <div class="form-group">
                <label for="">Hình ảnh</label>
                <input type="file" name="txtname" class="form-control" >
                </div>
                <div class="form-group">
                <label for="">Giá</label>
                <input type="number" name="txtname" class="form-control" >
                </div>
                <div class="form-group">
               <label for="">Mô tả ngắn</label>
               <textarea class="form-control" rows="5" cols="20" name="txtdes">
                </textarea>
              </div>
              <div class="form-group">
              <label for="">Mô tả </label>
                <textarea class="form-control" rows="10" cols="20" name="txtdes">
                </textarea>
              </div>
              <div class="form-group">
              <button type="submit" name="btnadd" class="btn btn-primary">Thêm sản phẩm</button>
              </div>
            </form>
            </div>
            
    </main>
</div>

      

</body>
</html>