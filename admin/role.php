<?php
  require_once ('../database/connect.php');
?>
<?php
if (isset($_GET['id'])) {
  $id = $_GET['id'];
  $stmt = $objConn->prepare("DELETE FROM role WHERE id = '$id'");
  $stmt->execute();
}
?>
<?php
    require_once ('header.php');
?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <!-- hien thi tung chuc nang cua trang quan tri START-->
        <?php
        try {
  $stmt = $objConn->prepare("SELECT * FROM role ORDER BY id ASC");
  // thuc thi cau lenh
  $stmt->execute();
  // thiet lap che do lay du lieu
  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  // tao bien luu tru du lieu;
  $dbrole = $stmt->fetchALL();
} 
catch (Exception $e) {
  echo "<br> Loi truy van CSDL"($e->getMessage());
}
?>
        <table class="table table-bordered" style="background-color: #fff;" class="mt-5">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">Role</th>
              <th scope="col">
                <a href="addRole.php" class="btn btn-primary">Thêm Role</a>
              </th>
            </tr>
          </thead>
          <tbody>
          <?php
                        
                        foreach ($dbrole as $item) {

                        ?>
                        
                            <tr>
                                <td><?= $item['id'] ?></td>
                                <td><?= $item['name'] ?></td>
                                <td>
                                    <a name="" id="" class="btn btn-warning" href="editRole.php?id=<?= $item['id'] ?>" role="button">Sửa</a>
                                    <a name="" id="" class="btn btn-danger" href="role.php?id=<?= $item['id'] ?>" role="button" onclick="return confirm('Bạn muốn xóa mục này?')">Xóa</a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
          </tbody>
        </table>
    </main>
</div>


      

</body>
</html>